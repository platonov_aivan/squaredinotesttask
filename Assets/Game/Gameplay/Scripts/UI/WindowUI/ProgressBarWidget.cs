﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Gameplay.UI.WindowUI
{
    public class ProgressBarWidget : MonoBehaviour
    {
        [SerializeField] public Image _bar;

        public virtual void SetProgress(float progress)
        {
            _bar.fillAmount = progress;
        }
    }
}