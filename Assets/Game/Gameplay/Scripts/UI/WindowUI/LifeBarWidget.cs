﻿using Game.Gameplay.Creatures.Enemy;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.UI.WindowUI
{
    public class LifeBarWidget : MonoBehaviour
    {
        [SerializeField] private ProgressBarWidget _lifeBar;
        [SerializeField] private HealthComponent _hp;

        private readonly CompositeDisposable _trash = new CompositeDisposable();
        private float _maxHp;
        
        private void Start()
        {
            if (_hp == null)
                _hp = GetComponentInParent<HealthComponent>();

            _maxHp = _hp.Health;
            _trash.Retain(_hp.SubscribeOnDie(OnDie));
            _trash.Retain(_hp.SubscribeOnChange(OnHpChanged));
        }
        
        
        private void OnDie()
        {
            gameObject.SetActive(false);
        }

        private void OnHpChanged(float hp)
        {
            var progress = hp / _maxHp;
            if (_lifeBar != null)
                _lifeBar.SetProgress(progress);
        }

        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}