﻿using System;
using Game.Gameplay.Utils;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Gameplay.Input
{
	public class InputManager : Singleton<InputManager>
	{
		public event Action OnLmbUp;
		public event Action OnLmbDown;

		public Vector2 TouchPosition => _heroInput.Touch.TouchPosition.ReadValue<Vector2>();
	
		public readonly Lock Locker = new Lock();
		
		private TouchControls _heroInput;

		public IDisposable SubscribeOnLmbUp(Action call)
		{
			OnLmbUp += call;
			return new ActionDisposable(() => OnLmbUp -= call);
		}
		public IDisposable SubscribeOnLmbDown(Action call)
		{
			OnLmbDown += call;
			return new ActionDisposable(() => OnLmbDown -= call);
		}
		
		protected override void Awake()
		{
			base.Awake();
			_heroInput = new TouchControls();
			OnLockChange(Locker.IsLocked);
		}
		private void Start()
		{
			Locker.OnChange += OnLockChange;
			_heroInput.Touch.TouchButton.started += TouchStart;
			_heroInput.Touch.TouchButton.canceled += TouchEnd;
		}

		private void OnLockChange(bool isLock)
		{
			if (!isLock)
			{
				_heroInput.Enable();
			}
			else
			{
				_heroInput.Disable();
			}
		}

		private void TouchStart(InputAction.CallbackContext obj)
		{
			OnLmbDown?.Invoke();
		}

		private void TouchEnd(InputAction.CallbackContext obj)
		{
			OnLmbUp?.Invoke();
		}
		
		protected override void OnDestroy()
		{
			base.OnDestroy();
			Locker.OnChange -= OnLockChange;
			_heroInput.Touch.TouchButton.started -= TouchStart;
			_heroInput.Touch.TouchButton.canceled -= TouchEnd;
		}
	}
}