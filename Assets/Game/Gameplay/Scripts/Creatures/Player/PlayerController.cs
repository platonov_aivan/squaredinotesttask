﻿using Game.Gameplay.Input;
using Game.Gameplay.Utils;
using Game.Gameplay.Utils.Disposables;
using Game.Gameplay.Utils.ObjectPool;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Gameplay.Creatures.Player
{
    public class PlayerController : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private NavMeshAgent _agentPlayer;
        [SerializeField] private Animator _animatorPlayer;
        [SerializeField] private Transform _heroHips;
        [Header("Movement")]
        [SerializeField] private float _stopVelocity = 0.2f;
        [Space]
        [Header("Bullet")]
        [SerializeField] private GameObject _bulletPrefab;
        [SerializeField] private Transform _bulletSpawnPoint;
        
        private static readonly int RunKey = Animator.StringToHash("run");
        private bool _isRun;
        private bool _isInputLocker;
        
        private readonly CompositeDisposable _trash = new CompositeDisposable();
        
        private void Start()
        {
           _trash.Retain(Locator.Level.LevelController.SubscribeOnWayPointRemove(OnRemoveWayPoint));
        }

        private void OnRemoveWayPoint()
        {
            var controlPoint = Locator.Level.LevelController.EnemyControllers[0].ControlPoint;
            Locator.Level.PlayerController.MoveToPoint(controlPoint);
        }
        
        
        private void Update()
        {
            UpdateRun();
            UpdateLockInputRun();
            void UpdateLockInputRun()
            {
                if (_isRun)
                {
                    if (_isInputLocker) return;
                    _isInputLocker = true;
                    InputManager.Instance.Locker.Retain(this);
                }
                else
                {
                    if (!_isInputLocker) return;
                    _isInputLocker = false;
                    InputManager.Instance.Locker.Release(this);
                }
            }
            void UpdateRun()
            {
                var isRun = _agentPlayer.velocity.magnitude > _stopVelocity;
                if (_isRun == isRun) return;
                _isRun = isRun;
                _animatorPlayer.SetBool(RunKey,isRun);
            }
        }

        private void MoveToPoint(Transform waypoint)
        {
            _agentPlayer.SetDestination(waypoint.position);
        }

        public void ShootToTarget(Vector3 targetShoot)
        {
            RotateToTarget(targetShoot);
            var bullet = Pool.Instance.Get(_bulletPrefab, _bulletSpawnPoint.position);
            var shootingComponent = bullet.GetComponent<ShootingComponent>();
            shootingComponent.Initialize(targetShoot);

            void RotateToTarget(Vector3 target)
            {
                var targetRotation = target - _heroHips.position;
                targetRotation.y = 0;
                var rotationPlayer = Quaternion.LookRotation(targetRotation);
                transform.rotation = rotationPlayer;
            }
        }

        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}