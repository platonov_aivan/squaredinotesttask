﻿using Game.Gameplay.Input;
using Game.Gameplay.Utils;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Player
{
	public class HeroInput : MonoBehaviour
	{
		[SerializeField] private LayerMask _terrainHitMask;
		[SerializeField] private LayerMask _enemyHitMask;

		private readonly CompositeDisposable _trash = new CompositeDisposable();
		
		private readonly RaycastHit[] _raycastHits = new RaycastHit[10];
		
		private bool _isFirstClick = true;
		
		private void Start()
		{
			_trash.Retain(InputManager.Instance.SubscribeOnLmbDown(OnLmbDown));
		}
		
		private void OnLmbDown()
		{
			if (_isFirstClick)
			{
				_isFirstClick = false;
				Locator.Level.LevelController.RemoveFirstWayPoint();
				return;
			}
			Shoot();
		}

		private void Shoot()
		{
			if (InputManager.Instance == null) return;
			var touchPos = InputManager.Instance.TouchPosition;
			var rayToMousePos = RayToMousePos(touchPos);
			
			var numEnemies = Physics.RaycastNonAlloc(rayToMousePos, _raycastHits, 100f, _enemyHitMask);
			if (numEnemies > 0)
			{
				Locator.Level.PlayerController.ShootToTarget(_raycastHits[0].point);
				return;
			}
			
			var numTerrains = Physics.RaycastNonAlloc(rayToMousePos, _raycastHits, 100f, _terrainHitMask);
			if (numTerrains > 0)
			{
				Locator.Level.PlayerController.ShootToTarget(_raycastHits[0].point);
				return;
			}
			
			Ray RayToMousePos(Vector2 touchPosition)
			{
				var rayStart = Locator.Level.MainCamera.transform.position;
				var rayEnd = Locator.Level.MainCamera.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, 10f));
				var rayDir = rayEnd - rayStart;
				return new Ray(rayStart, rayDir);
			}
		}

		private void OnDestroy()
		{
			_trash.Dispose();
		}
	}
}