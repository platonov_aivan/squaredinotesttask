﻿using Game.Gameplay.Colliders.Triggers;
using Game.Gameplay.Creatures.Enemy;
using Game.Gameplay.Utils;
using Game.Gameplay.Utils.ObjectPool;
using UnityEngine;

namespace Game.Gameplay.Creatures.Player
{
    public class ShootingComponent : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private EnterTrigger _trigger;
        [SerializeField] private PoolItem _poolItem;
        [SerializeField] private Collider _collider;
        [SerializeField] private Rigidbody _rigidbody;
        [Header("Settings")]
        [SerializeField] public float _speed;
        [SerializeField] private int _damage;
        [SerializeField] private float _bulletLifeTime = 4f;
        [SerializeField] private LayerMask _enemyHit;
        
        
        
        private float _timeSpawn;
        private Vector3 _moveDirection;

        private void Awake()
        {
            _trigger.OnInteractGameObject += OnInteract;
            _poolItem.OnRestart += OnRestart;
        }
        private void OnRestart()
        {
            _collider.enabled = true;
        }

        private void OnInteract(GameObject obj)
        {
            if (obj.gameObject.IsInLayer(_enemyHit))
            {
                var enemyHealth = obj.GetComponentInParent<HealthComponent>();
                enemyHealth.ChangeHealth(_damage);
            }
            _collider.enabled = false;
            _poolItem.Release();
        }

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(_rigidbody.position + _moveDirection);
            
            if (_timeSpawn + _bulletLifeTime < Time.time)
            {
                _poolItem.Release();
            }
        }

        public void Initialize(Vector3 shootTarget)
        {
            _moveDirection = shootTarget - _rigidbody.position;
            _moveDirection = _moveDirection.normalized;
            _moveDirection *= _speed * Time.fixedDeltaTime;
            RotateToTarget(_moveDirection);
            _timeSpawn = Time.time;

            void RotateToTarget(Vector3 direction)
            {
                var targetRotation = direction.normalized;
                var rotation = Quaternion.LookRotation(targetRotation);
                transform.rotation = rotation;
            }
        }

        private void OnDestroy()
        {
            _trigger.OnInteractGameObject -= OnInteract;
            _poolItem.OnRestart -= OnRestart;
        }
    }
}