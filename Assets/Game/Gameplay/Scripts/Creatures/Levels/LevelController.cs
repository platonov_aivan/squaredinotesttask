﻿using System;
using System.Collections.Generic;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Levels
{
    public class LevelController : MonoBehaviour
    {
        public event Action OnWayPointRemove;
        public List<EnemyListController> EnemyControllers => _enemyControllers;
        
        private readonly List<EnemyListController> _enemyControllers = new List<EnemyListController>();
        
        private readonly CompositeDisposable _trash = new CompositeDisposable();
        
        public IDisposable SubscribeOnWayPointRemove(Action call)
        {
            OnWayPointRemove += call;
            return new ActionDisposable(() => OnWayPointRemove -= call);
        }
        
        public void RemoveFirstWayPoint()
        {
            OnRemoveWaypoints(_enemyControllers[0]);
        }
        
        private void Awake()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var enemyListController = transform.GetChild(i).GetComponent<EnemyListController>();
                if (enemyListController != null)
                {
                    _enemyControllers.Add(enemyListController);
                }
            }
        }

        private void Start()
        {
            foreach (var enemyWaypoints in _enemyControllers)
            {
                _trash.Retain(enemyWaypoints.SubscribeOnRemovePoint(OnRemoveWaypoints));
            }
        }

        private void OnRemoveWaypoints(EnemyListController enemyListController)
        {
            _enemyControllers.Remove(enemyListController);
            OnWayPointRemove?.Invoke();
        }
        
        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}