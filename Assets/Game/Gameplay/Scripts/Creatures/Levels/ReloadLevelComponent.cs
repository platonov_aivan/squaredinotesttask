﻿using Game.Gameplay.Colliders.Triggers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Gameplay.Creatures.Levels
{
    public class ReloadLevelComponent : MonoBehaviour
    {
        [SerializeField] private EnterTrigger _enterTrigger;
        private void Awake()
        {
            _enterTrigger.OnInteract += OnInteract;
        }

        private void OnInteract()
        {
            SceneManager.LoadScene("MainScene");
        }
        
        private void OnDestroy()
        {
            _enterTrigger.OnInteract -= OnInteract;
        }
    }
}