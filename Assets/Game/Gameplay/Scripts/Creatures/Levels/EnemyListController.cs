﻿using System;
using System.Collections.Generic;
using Game.Gameplay.Creatures.Enemy;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Levels
{
    public class EnemyListController : MonoBehaviour
    {
        public event Action<EnemyListController> OnRemovePoint;
        
        [SerializeField] private Transform _controlPoint;
        [SerializeField] private Transform _enemyContainer;
        [SerializeField] private int _enemyCounter;

        public Transform ControlPoint => _controlPoint;
        
        private readonly List<HealthComponent>  _containerEnemiesOnWaypoints = new List<HealthComponent>();
        
        private readonly CompositeDisposable _trash = new CompositeDisposable();
        
        public IDisposable SubscribeOnRemovePoint(Action<EnemyListController> call)
        {
            OnRemovePoint += call;
            return new ActionDisposable(() => OnRemovePoint -= call);
        }
        
        private void Awake()
        {
            for (int i = 0; i < _enemyContainer.childCount; i++)
            {
                var health = _enemyContainer.GetChild(i).GetComponent<HealthComponent>();
                if (health != null)
                {
                    _containerEnemiesOnWaypoints.Add(health);
                }
            }
            foreach (var enemy in _containerEnemiesOnWaypoints)
            {
                _trash.Retain(enemy.SubscribeOnDie(OnEnemyDied));
                _enemyCounter++;
            }
        }

        private void OnEnemyDied()
        {
            _enemyCounter--;
            if (_enemyCounter == 0)
            {
                OnRemovePoint?.Invoke(this);
                enabled = false;
            }
        }
        
        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}