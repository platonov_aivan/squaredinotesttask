﻿using Game.Gameplay.Creatures.Player;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Levels
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private Camera _mainCamera;
        public Camera MainCamera => _mainCamera;

        [SerializeField] private Transform _playerPosition;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private LevelController _levelController;
        
        public LevelController LevelController => _levelController;

        public  PlayerController PlayerController => _playerController;
        
        private Transform _cachedHeroTransform;
        
        private readonly CompositeDisposable _trash = new CompositeDisposable();
        

        private void OnHeroDie()
        {
            LoseLevel();
        }
        
        private void LoseLevel()
        {
            StartCoroutine(LoseCoroutine());
        }

        private string LoseCoroutine()
        {
            throw new System.NotImplementedException();
        }


        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}