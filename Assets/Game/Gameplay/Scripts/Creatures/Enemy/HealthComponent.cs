﻿using System;
using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Enemy
{
    public class HealthComponent : MonoBehaviour
    {
        public event Action OnDie;
        public event Action OnDamage;
        public event Action OnHeal;
        public event Action<float> OnHpChanged;
        
        [SerializeField] private float _health;
        
        public float Health => _health;

        public IDisposable SubscribeOnDie(Action call)
        {
            OnDie += call;
            return new ActionDisposable(() => OnDie -= call);
        }
        public IDisposable SubscribeOnDamage(Action call)
        {
            OnDamage += call;
            return new ActionDisposable(() => OnDamage -= call);
        }
        public IDisposable SubscribeOnHeal(Action call)
        {
            OnHeal += call;
            return new ActionDisposable(() => OnHeal -= call);
        }
        public IDisposable SubscribeOnChange(Action<float> call)
        {
            OnHpChanged += call;
            return new ActionDisposable(() => OnHpChanged -= call);
        }


        public void ChangeHealth(float deltaHealth)
        {
            if (_health <= 0) return;
            
            _health += deltaHealth;
            OnHpChanged?.Invoke(_health);
            
            if (deltaHealth <0)
            {
                OnDamage?.Invoke();

            } else if (deltaHealth>0)
            {
                OnHeal?.Invoke();
            } 
            if (_health <= 0)
            {
                OnDie?.Invoke();
                
                _health = 0;
            }
        }


#if UNITY_EDITOR
        [ContextMenu("Update Health")]
        private void UpdateHealth()
        {
            OnHpChanged?.Invoke(_health);
            OnDie?.Invoke();
        }
        [ContextMenu("Kill")]
        private void Kill()
        {
            ChangeHealth(_health * -1);
        }
#endif
        public void SetHealth(float health)
        {
            _health = health;
        }
    }
}

