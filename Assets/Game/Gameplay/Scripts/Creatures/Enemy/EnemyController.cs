﻿using Game.Gameplay.Utils.Disposables;
using UnityEngine;

namespace Game.Gameplay.Creatures.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Animator _animatorEnemy;
        [SerializeField] private HealthComponent _healthComponentEnemy;
        
        private CompositeDisposable _trash = new CompositeDisposable();

        private void Awake()
        {
            _trash.Retain(_healthComponentEnemy.SubscribeOnDie(OnDie));
        }
        
        private void OnDie()
        {
            _animatorEnemy.enabled = false;
        }

        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}