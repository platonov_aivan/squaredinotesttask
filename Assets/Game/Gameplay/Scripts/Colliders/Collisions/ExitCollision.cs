﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Collisions
{
	public class ExitCollision : BasePhysicsInteract
	{
		private void OnCollisionExit(Collision other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}