﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Collisions
{
	public class StayCollision : BasePhysicsInteract
	{
		private void OnCollisionStay(Collision collisionInfo)
		{
			PhysicsInteract(collisionInfo.gameObject);
		}
	}
}