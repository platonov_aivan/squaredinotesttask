﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Collisions
{
	public class EnterCollision : BasePhysicsInteract
	{
		private void OnCollisionEnter(Collision collision)
		{
			PhysicsInteract(collision.gameObject);
		}
	}
}