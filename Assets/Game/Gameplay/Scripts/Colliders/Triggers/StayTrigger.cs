﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Triggers
{
	public class StayTrigger : BasePhysicsInteract
	{
		private void OnTriggerStay(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}