﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Triggers
{
	public class EnterTrigger : BasePhysicsInteract
	{
		private void OnTriggerEnter(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}