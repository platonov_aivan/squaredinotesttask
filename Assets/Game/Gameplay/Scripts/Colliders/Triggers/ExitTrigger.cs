﻿using UnityEngine;

namespace Game.Gameplay.Colliders.Triggers
{
	public class ExitTrigger : BasePhysicsInteract
	{
		private void OnTriggerExit(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}