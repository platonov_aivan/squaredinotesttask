﻿namespace Game.Gameplay.Utils.StateMachineBase
{
    public abstract class State<TTargetType>
    {
        protected StateMachine<TTargetType> StateMachine;

        protected TTargetType Target;

        public bool IsCurrentState { get; protected set;}

        protected State(TTargetType target, StateMachine<TTargetType> stateMachine)
        {
            Target = target;
            StateMachine = stateMachine;
        }

        public virtual void Init(TTargetType target, StateMachine<TTargetType> stateMachine)
        {
            Target = target;
            StateMachine = stateMachine;
        }

        public virtual void Enter()
        {
            IsCurrentState = true;
        }

        public virtual void LogicUpdate()
        {

        }

        public virtual void PhysicsUpdate()
        {

        }

        public virtual void Exit()
        {
            IsCurrentState = false;
        }

        public virtual void Destroy()
        {
			
        }
    }
}