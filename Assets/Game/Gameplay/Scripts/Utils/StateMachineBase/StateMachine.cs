﻿namespace Game.Gameplay.Utils.StateMachineBase
{
    public abstract class StateMachine<TTargetType>
    {
        public State<TTargetType> CurrentState { get; private set; }
		
		
        public void Initialize(State<TTargetType> startingState)
        {
            CurrentState = startingState;
            startingState.Enter();
        }

        public void ChangeState(State<TTargetType> newState)
        {
            CurrentState.Exit();

            CurrentState = newState;
            newState.Enter();
        }

        public void Stop()
        {
            CurrentState.Exit();
            CurrentState = null;
        }
    }
}