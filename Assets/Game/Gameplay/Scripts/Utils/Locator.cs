﻿using Game.Gameplay.Creatures.Levels;
using UnityEngine;

namespace Game.Gameplay.Utils
{
    public static class Locator
    {
        public static Level Level
        {
            get
            {
                if (_level != null) return _level;
                _level = Object.FindObjectOfType<Level>();
                return _level;
            }
        }
        private static Level _level;
    }
}