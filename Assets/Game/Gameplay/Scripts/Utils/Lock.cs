﻿using System;
using System.Collections.Generic;
using Object = System.Object;

namespace Game.Gameplay.Utils
{
    public class Lock
    {
        public event Action<bool> OnChange;
        public bool IsLocked => _retained.Count > 0;

        private readonly List<Object> _retained = new List<object>();

        public void Retain(Object item)
        { 
            _retained.Add(item);
            OnChange?.Invoke(IsLocked);
        }

        public void Release(object item)
        {
            _retained.Remove(item);
            OnChange?.Invoke(IsLocked);
        }
    }
}